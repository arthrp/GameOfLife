﻿using System;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            new Game(50, 20).Run();
        }
    }
}
