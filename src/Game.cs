using System;
using System.Text;
using System.Threading;

namespace GameOfLife
{
    public class Game
    {
        private readonly int _width;
        private readonly int _height;
        private bool[,] _board;

        public Game(int width, int height)
        {
            _width = width;
            _height = height;

            _board = new bool[width, height];
            Init();
        }

        public void Run()
        {
            while(true)
            {
                Print();
                MakeTurn();
                Thread.Sleep(500);
            }
        }

        public void Print()
        {
            Console.Clear();
            var s = new StringBuilder();
            for(int y = 0; y < _height; y++){
                for(int x = 0; x < _width; x++){                    
                    s.Append(_board[x,y] ? "■" : "□");
                }
                s.Append(Environment.NewLine);
            }
            Console.Write(s.ToString());
        }

        private void Init()
        {
            //Blinker
            _board[2,2] = true;
            _board[2,3] = true;
            _board[2,4] = true;

            //Glider
            _board[7,6] = true;
            _board[8,7] = true;
            _board[6,8] = true;
            _board[7,8] = true;
            _board[8,8] = true;
        }
        
        private int GetNeighbours(int cellX, int cellY)
        {
            int neighbourCount = 0;

            for(var deltaY = -1; deltaY <= 1; deltaY++)
            {
                //Loop around the edges (so that board is infinite)
                var y = (cellY + deltaY + _height) % _height;

                for(var deltaX = -1; deltaX <= 1; deltaX++)
                {
                    int x = (cellX + deltaX + _width) % _width;
                    neighbourCount += _board[x,y] ? 1 : 0;
                }
            }

            //Not including cell itself
            return neighbourCount - (_board[cellX,cellY] ? 1 : 0);
        }

        private void MakeTurn()
        {
            var newBoard = new bool[_width, _height];

            for(var y = 0; y < _height; y++)
            {
                for(var x = 0; x < _width; x++)
                {
                    var n = GetNeighbours(x,y);
                    var currentCell = _board[x,y];

                    //Cell remains alive if it has 2 or 3 neighbours. Dead cell comes to life when it has exactly 3 neighbours.
                    newBoard[x,y] = (currentCell && (n == 2 || n == 3)) || (!currentCell && n == 3);
                }
            }

            _board = newBoard;
        }
    }
}